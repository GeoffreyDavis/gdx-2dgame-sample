/**
 * @file GdxGame2dSample.java
 *
 * Written in 2017 by Geoffrey Davis <me@geoffreydavis.com>
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along with this software.
 * If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 *
 * @package com.geoffreydavis.gdxsample.game2d
 * @author Geoffrey Davis
 */

package com.geoffreydavis.gdxsample.game2d;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntSet;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * A simple 2D game template using libgdx.
 * @author Geoffrey Davis
 */
public class GdxGame2dSample extends ApplicationAdapter {
	/** The number of tiles walked per second. */
	public final static float WALKING_SPEED = 3.0f;

	/** Clamps the {@link Camera} position. */
	private void clampCamera() {
		// Retrieve the world size.
		final int worldWidth = mTiledMap.getProperties().get("width", Integer.class);
		final int worldHeight = mTiledMap.getProperties().get("height", Integer.class);

		// Retrieve the camera we're going to use for our calculations.
		mCamera.zoom = MathUtils.clamp(mCamera.zoom, 0.1f, 100f / mCamera.viewportWidth);

		// Calculate the effective size of the viewport.
		float effectiveViewportWidth = mCamera.viewportWidth * mCamera.zoom;
		float effectiveViewportHeight = mCamera.viewportHeight * mCamera.zoom;

		mCamera.position.x = MathUtils.clamp(mCamera.position.x, effectiveViewportWidth / 2f, worldWidth - effectiveViewportWidth / 2f);
		mCamera.position.y = MathUtils.clamp(mCamera.position.y, effectiveViewportHeight / 2f, worldHeight - effectiveViewportHeight / 2f);
		mCamera.update();
	}

	/** Clamps the player position to the world. */
	private void clampPlayer() {
		if (mPlayer != null) {
			// Retrieve the world size.
			final float worldWidth = mTiledMap.getProperties().get("width", 0, Integer.class);
			final float worldHeight = mTiledMap.getProperties().get("height", 0, Integer.class);

			mPlayer.setPosition(
					MathUtils.clamp(mPlayer.getX(), 0.0f, worldWidth - 1.0f),
					MathUtils.clamp(mPlayer.getY(), 0.0f, worldHeight - 1.0f));
		}
	}

	/** Do player animation setup. */
	private void configureAnimations() {
		// Load the texture atlas.
		mTextureAtlas = new TextureAtlas("image/player.atlas");
		final String[] animationNames = {
				"walk-north", "walk-east", "walk-south", "walk-west"
		};

		// Create the player animations.
		for (final String animationName : animationNames) {
			final Array<TextureAtlas.AtlasRegion> regions = mTextureAtlas.findRegions(animationName);
			final Animation<TextureRegion> animation = new Animation<TextureRegion>(
					1.0f / WALKING_SPEED / regions.size, regions, Animation.PlayMode.LOOP);
			mAnimations.put(animationName, animation);
		}
	}

	/** Do camera setup. */
	private void configureCamera() {
		// Create the new camera object.
		mCamera = new OrthographicCamera();
		mCamera.setToOrtho(false, 20f, 12.5f);
		mCamera.update();
	}

	/** Do input processor setup. */
	private void configureInputProcessor() {
		Gdx.input.setInputProcessor(new InputAdapter() {
			@Override
			public boolean keyDown(final int keycode) {
				switch (keycode) {
					case Input.Keys.DOWN:
					case Input.Keys.S:
						mAnimationTime = 0.0f;
						mKeysPressed.add(Input.Keys.DOWN);
						return true;
					case Input.Keys.LEFT:
					case Input.Keys.A:
						mAnimationTime = 0.0f;
						mKeysPressed.add(Input.Keys.LEFT);
						return true;
					case Input.Keys.RIGHT:
					case Input.Keys.D:
						mAnimationTime = 0.0f;
						mKeysPressed.add(Input.Keys.RIGHT);
						return true;
					case Input.Keys.UP:
					case Input.Keys.W:
						mAnimationTime = 0.0f;
						mKeysPressed.add(Input.Keys.UP);
						return true;
					case Input.Keys.SHIFT_LEFT:
					case Input.Keys.SHIFT_RIGHT:
						mKeysPressed.add(Input.Keys.SHIFT_LEFT);
						mKeysPressed.add(Input.Keys.SHIFT_RIGHT);
						return true;
				}
				return false;
			}

			@Override
			public boolean keyUp(final int keycode) {
				switch (keycode) {
					case Input.Keys.DOWN:
					case Input.Keys.S:
						mAnimationTime = 0.0f;
						mKeysPressed.remove(Input.Keys.DOWN);
						return true;
					case Input.Keys.LEFT:
					case Input.Keys.A:
						mAnimationTime = 0.0f;
						mKeysPressed.remove(Input.Keys.LEFT);
						return true;
					case Input.Keys.RIGHT:
					case Input.Keys.D:
						mAnimationTime = 0.0f;
						mKeysPressed.remove(Input.Keys.RIGHT);
						return true;
					case Input.Keys.UP:
					case Input.Keys.W:
						mAnimationTime = 0.0f;
						mKeysPressed.remove(Input.Keys.UP);
						return true;
					case Input.Keys.SHIFT_LEFT:
					case Input.Keys.SHIFT_RIGHT:
						mKeysPressed.remove(Input.Keys.SHIFT_LEFT);
						mKeysPressed.remove(Input.Keys.SHIFT_RIGHT);
						return true;
				}
				return false;
			}
		});
	}

	/** Do player sprite setup. */
	private void configurePlayerSprite() {
		// Create the player texture and sprite.
		mPlayer = new Sprite(mAnimations.get("walk-south").getKeyFrame(0.0f));
		mPlayer.setPosition(
				mTiledMap.getProperties().get("width", Integer.class) / 2.0f,
				mTiledMap.getProperties().get("height", Integer.class) / 2.0f);
		mPlayer.setOrigin(0.5f, 0f);

		// Calculate the scaling factor for the sprite.
		// Should always be the same as the unitScale used for the TMX tiled map.
		mPlayer.setScale(1.0f / Math.min(
				mTiledMap.getProperties().get("tilewidth", Integer.class),
				mTiledMap.getProperties().get("tileheight", Integer.class)));

		// The camera follows the player.
		mCamera.position.x = mPlayer.getX();
		mCamera.position.y = mPlayer.getY();
		mCamera.update();
	}

	/** Do TMX tiled map setup. */
	private void configureTiledMap() {
		mTiledMap = new TmxMapLoader().load("map/tiledmap.tmx");
	}

	/** Do tiled map renderer setup */
	private void configureTiledMapRenderer() {
		// Calculate the tile "size" from its width and height.
		final int tileSize = Math.min(
				mTiledMap.getProperties().get("tilewidth", Integer.class),
				mTiledMap.getProperties().get("tileheight", Integer.class));

		// The number of pixels in the smaller size of the tile determines the unit scale.
		mTiledMapRenderer = new OrthogonalTiledMapRenderer(mTiledMap, 1f / tileSize);
	}

	@Override
	public void create() {
		configureTiledMap();
		configureTiledMapRenderer();
		configureCamera();
		configureAnimations();
		configurePlayerSprite();
		configureInputProcessor();
	}

	@Override
	public void dispose() {
		if (mPlayer != null)
			mPlayer.getTexture().dispose();
		if (mTextureAtlas != null)
			mTextureAtlas.dispose();
		if (mTiledMapRenderer != null)
			mTiledMapRenderer.dispose();
		if (mTiledMap != null)
			mTiledMap.dispose();
	}

	private volatile float mAnimationTime = 0.0f;

	@Override
	public void render() {
		// Begin with the standard walking speed.
		float speed = WALKING_SPEED;

		// How much time has passed.
		final float timeDelta = Gdx.graphics.getDeltaTime();
		mAnimationTime += timeDelta;

		if (mKeysPressed.contains(Input.Keys.SHIFT_LEFT)) {
			speed *= 2.0f;
		}
		if (mKeysPressed.contains(Input.Keys.DOWN)) {
			mPlayer.setRegion(mAnimations.get("walk-south").getKeyFrame(mAnimationTime));
			mPlayer.setPosition(mPlayer.getX(), mPlayer.getY() - speed * timeDelta);
		}
		if (mKeysPressed.contains(Input.Keys.LEFT)) {
			mPlayer.setRegion(mAnimations.get("walk-west").getKeyFrame(mAnimationTime));
			mPlayer.setPosition(mPlayer.getX() - speed * timeDelta, mPlayer.getY());
		}
		if (mKeysPressed.contains(Input.Keys.RIGHT)) {
			mPlayer.setRegion(mAnimations.get("walk-east").getKeyFrame(mAnimationTime));
			mPlayer.setPosition(mPlayer.getX() + speed * timeDelta, mPlayer.getY());
		}
		if (mKeysPressed.contains(Input.Keys.UP)) {
			mPlayer.setRegion(mAnimations.get("walk-north").getKeyFrame(mAnimationTime));
			mPlayer.setPosition(mPlayer.getX(), mPlayer.getY() + speed * timeDelta);
		}

		// Clamp the player's location.
		clampPlayer();

		// The camera always follows the player.
		mCamera.position.set(mPlayer.getX(), mPlayer.getY(), 0.0f);
		mCamera.update();

		// Clamp the camera's location.
		clampCamera();

		// Clear the screen before we draw.
		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// Render the tiled map.
		mTiledMapRenderer.setView(mCamera);
		mTiledMapRenderer.render();

		// Render sprites.
		mTiledMapRenderer.getBatch().begin();
		if (mPlayer != null) {
			mPlayer.draw(mTiledMapRenderer.getBatch());
		}
		mTiledMapRenderer.getBatch().end();
	}

	/** The player animations. */
	private final ObjectMap<String, Animation<TextureRegion>> mAnimations =
			new ObjectMap<String, Animation<TextureRegion>>();

	/** The texture atlas containing the player animations. */
	private TextureAtlas mTextureAtlas;

	/** The {@link OrthographicCamera} object. */
	private OrthographicCamera mCamera;

	/** Records the state of the keypresses we care about. */
	private final IntSet mKeysPressed = new IntSet();

	/** The player {@link Sprite}. */
	private Sprite mPlayer;

	/** The TMX tiled map object. */
	private TiledMap mTiledMap;

	/** The renderer object. */
	private OrthogonalTiledMapRenderer mTiledMapRenderer;

}
